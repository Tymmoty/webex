import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';

import { NotifierModule } from 'angular-notifier';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { NgHttpLoaderModule } from 'ng-http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { customNotifierOptions } from './notifier.options';
import { SharedModule } from './shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './shared/interceptors/auth.interceptor';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		NotifierModule.withConfig(customNotifierOptions),
		SharedModule,
		DeviceDetectorModule.forRoot(),
		NgHttpLoaderModule.forRoot(),
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
		},
		{
			provide: LOCALE_ID,
			useValue: "nl-NL"
		},
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
