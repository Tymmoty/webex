import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { Message } from "../models/message";


@Injectable({
	providedIn: 'root',
})
export class MessageService {

	private validationSubject: Subject<Message> = new Subject<Message>();
	private errorSubject: Subject<Message> = new Subject<Message>();
	private createdSubject: Subject<Message> = new Subject<Message>();


	public notifyValidation(title: string, body: string) {

		let message = new Message(title, body);

		message.title = title;
		message.body = body;

		this.validationSubject.next(message);
	}

	public getValidationMessages(): Observable<Message> {

		return this.validationSubject.asObservable();
	}

	public notifyError(title: string, body: string, error: string) {

		let message = new Message(title, body);

		message.title = title;
		message.body = body;
		message.error = error;

		this.errorSubject.next(message);
	}

	public getErrorMessages(): Observable<Message> {

		return this.errorSubject.asObservable();
	}

	public notifyCreated(title: string, body: string) {

		let messge = new Message(title, body);

		messge.title = title;
		messge.body = body;

		this.createdSubject.next(messge);
	}

	public getCreatedMessages(): Observable<Message> {

		return this.createdSubject.asObservable();
	}
}