export class Message {
	constructor(public title: string, public body: string, public error: string = null) {

	}
}