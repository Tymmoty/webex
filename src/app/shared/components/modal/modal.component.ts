import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { MessageService } from '../../services/message.service';

@Component({
	selector: 'avit-modal',
	templateUrl: './modal.component.html',
	styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

	@Input() modalTitle: string = 'Avit - Webex';

	showErrors: boolean;
	error: string;

	constructor(public activeModal: NgbActiveModal,
		private messageService: MessageService) {

	}

	ngOnInit(): void {

		this.messageService.getValidationMessages().subscribe((e) => {
			this.showErrors = true;
			this.error = e.body
		})
	}
}
