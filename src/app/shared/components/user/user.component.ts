import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'avit-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.scss'],
	host: { 'class': 'd-flex align-items-center' }
})
export class UserComponent implements OnInit {

	name: string;

	constructor() {

		this.name = 'Test Gebruiker';
	}

	ngOnInit() {

	}
}