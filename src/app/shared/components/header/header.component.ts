import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'avit-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
	host: { 'class': 'page-header flex-column flex-lg-row pl-lg-5 pr-lg-5' }
})
export class HeaderComponent implements OnInit {

	@Input() isAuthenticated: boolean;

	ngOnInit() {

	}
}