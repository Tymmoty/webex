import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import {
	faSignOutAlt, faCaretSquareDown, faClock, faAngleDown, faUserAlt, faPlay, faCheck, faVideo, faAngleLeft, faPen, faAngleRight, faCircle, faCircleNotch, faTimes, faSpinner, faCalendar, faCalendarAlt, faCalendarWeek, faPaperclip, faAngleUp, faHome, faInfo
} from '@fortawesome/free-solid-svg-icons';

import { HeaderComponent } from './components/header/header.component';
import { ModalComponent } from './components/modal/modal.component';
import { UserComponent } from './components/user/user.component';
import { SafePipe } from './pipes/safe.pipe';
import { StopPropagationDirective } from './directives/stop-propagation.directive';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,
		BrowserModule,
		FontAwesomeModule,
		NgbModule,
		ReactiveFormsModule,
	],
	declarations: [
		HeaderComponent,
		ModalComponent,
		SafePipe,
		StopPropagationDirective,
		UserComponent
	],
	exports: [
		BrowserModule,
		FontAwesomeModule,
		FormsModule,
		HeaderComponent,
		ModalComponent,
		NgbModule,
		ReactiveFormsModule,
		SafePipe,
		StopPropagationDirective
	],
	providers: [
		NgbActiveModal
	]
})
export class SharedModule {
	constructor(private library: FaIconLibrary) {

		library.addIcons(
			faAngleDown,
			faCaretSquareDown,
			faUserAlt,
			faPlay,
			faCheck,
			faVideo,
			faAngleLeft,
			faPen,
			faAngleRight,
			faAngleUp,
			faHome,
			faCircle,
			faCircleNotch,
			faSignOutAlt,
			faClock,
			faSpinner,
			faTimes,
			faCalendar,
			faCalendarAlt,
			faCalendarWeek,
			faPaperclip,
			faInfo
		);
	}
}