import { Directive, HostListener } from "@angular/core";

@Directive({
	selector: '[stop-propagation]'
})
export class StopPropagationDirective {

	@HostListener('click', ['$event'])
	preventClick(event: any) {

		event.preventDefault();
		event.stopPropagation();
	}
}